import { EmployeeDto } from "./employeeDto";

export class EmployeesResponseDto {
    status: string | undefined;
    data: Array<EmployeeDto> | undefined;
    message: string | undefined;
}