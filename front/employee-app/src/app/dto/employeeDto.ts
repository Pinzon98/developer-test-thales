export class EmployeeDto {
    id: number | undefined;
    employee_name: string | undefined;
    employee_salary: number | undefined;
    employee_anual_salary: number | undefined;
    employee_age: number | undefined;
    profile_image: string | undefined;
}