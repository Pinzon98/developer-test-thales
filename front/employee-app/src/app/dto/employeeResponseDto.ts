import { EmployeeDto } from "./employeeDto";

export class EmployeeResponseDto {
    status: string | undefined;
    data: EmployeeDto | undefined;
    message: string | undefined;
}