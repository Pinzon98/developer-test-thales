import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { EmployeesResponseDto } from '../dto/employeesResponseDto';
import { EmployeeResponseDto } from '../dto/employeeResponseDto';

@Injectable({
  providedIn: 'root'
})
export class EmployeeService {

  private baseUrl = 'http://localhost:8080/employees';

  constructor(private http: HttpClient) { }

  getEmployees(): Observable<EmployeesResponseDto> {
    return this.http.get<EmployeesResponseDto>(`${this.baseUrl}`);
  }

  getEmployeeById(id: number): Observable<EmployeeResponseDto> {
    return this.http.get<EmployeeResponseDto>(`${this.baseUrl}/${id}`);
  }
}
