import { Component, OnInit } from '@angular/core';
import { EmployeeService } from '../service/employee.service';
import { EmployeeDto } from '../dto/employeeDto';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

@Component({
  selector: 'app-employee-search',
  standalone: true,
  imports: [CommonModule, FormsModule],
  templateUrl: './employee-search.component.html',
  styleUrl: './employee-search.component.css'
})
export class EmployeeSearchComponent implements OnInit {

  employees: Array<EmployeeDto> = [];
  searchId: string = '';
  errorMsg: string | undefined;

  constructor(private employeeService: EmployeeService) { }

  ngOnInit(): void {
    this.getAllEmployees();
  }

  getAllEmployees(): void {
    this.employeeService.getEmployees().subscribe(data => {
      if(data.status == "success"){
        this.employees = data.data ?? [];
        this.errorMsg = '';
      }
      else if(data.status == "faild"){
        this.employees = [];
        this.errorMsg = data.message;
      }
    });
  }

  searchEmployee(): void {
    if (this.searchId.trim() === '') {
      this.getAllEmployees();
    } else {
      const id = +this.searchId;
      if (!isNaN(id)) {
        this.employeeService.getEmployeeById(id).subscribe(employee => {
          if(employee.data && employee.status == "success"){
            this.employees?.push(employee.data);
            this.errorMsg = '';
          }
          else if(employee.status == "faild"){
            this.employees = [];
            this.errorMsg = employee.message;
          }
        }, error => {
          this.employees = [];
        });
      } else {
        this.employees = [];
      }
    }
  }

}
