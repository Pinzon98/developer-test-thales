# Java Software Engineer Developer Test

## To Begin

- The Frontend is built with Angular 16
- The Backend is build with Java 21 and Springboot 3.3.0
- Download the repository

- Angular Project (Frontend):
  1. Open the frontend in vsc
  2. Run the command npm i
  3. Run the command ng s
  4. Open the link http://localhost:4200/ in your preferred browser

- Java Project (Backend)
  1. Open the backend in an IDE like Intelij
  2. Download all mvn dependencies
  3. Go to back/employee/src/main/java/com.employee.employee/EmployeeApplication and run the main application or you can run the war in wildfly.
  4. Make sure backend is running on port 8080

- Design Patterns: 
  1. MVC
  2. DAO