package com.employee.employee.service;

import com.employee.employee.client.EmployeeClient;
import com.employee.employee.dto.EmployeeDto;
import com.employee.employee.dto.EmployeeResponseDto;
import com.employee.employee.dto.EmployeesResponseDto;
import com.employee.employee.service.impl.EmployeeService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

public class EmployeeServiceTest {

    @InjectMocks
    private EmployeeService employeeService;

    @Mock
    private EmployeeClient employeeClient;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    public void testCalculateAnnualSalary() {
        // Arrange
        double monthlySalary = 5000;
        double expectedAnnualSalary = monthlySalary * 12.0;

        // Act
        double annualSalary = employeeService.calculateAnualSalary(monthlySalary);

        // Assert
        assertEquals(expectedAnnualSalary, annualSalary);
    }

    @Test
    public void testGetAllEmployees() {
        // Arrange
        EmployeeDto employee1 = new EmployeeDto(1L, "John Doe", (double)5000, null, 30, "");
        EmployeeDto employee2 = new EmployeeDto(2L, "Jane Doe", (double)6000, null, 25, "");
        List<EmployeeDto> expectedEmployees = Arrays.asList(employee1, employee2);
        EmployeesResponseDto employeeResponseDto = new EmployeesResponseDto();
        employeeResponseDto.setData(expectedEmployees);

        when(employeeClient.getAllEmployees()).thenReturn(employeeResponseDto);

        // Act
        EmployeesResponseDto actualEmployees = employeeService.getAllEmployees();

        // Assert
        assertEquals(expectedEmployees, actualEmployees.getData());
    }

    @Test
    public void testGetEmployeeById() {
        // Arrange
        EmployeeDto expectedEmployee = new EmployeeDto(1L, "John Doe", (double)5000, null, 30, "");
        EmployeeResponseDto employeeResponseDto = new EmployeeResponseDto();
        employeeResponseDto.setData(expectedEmployee);

        when(employeeClient.getEmployeeById(1L)).thenReturn(employeeResponseDto);

        // Act
        EmployeeResponseDto actualEmployee = employeeService.getEmployeeById(1L);

        // Assert
        assertEquals(expectedEmployee, actualEmployee.getData());
    }
}
