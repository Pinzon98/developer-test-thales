package com.employee.employee.model;

import lombok.*;

@Data
@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class Employee {

    private Long employeeId;

    private String employeeName;

    private Double employeeSalary;

    private Double employeeAnualSalary;
}
