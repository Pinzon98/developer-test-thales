package com.employee.employee.dto;

import lombok.Data;

@Data
public class EmployeeResponseDto {

    private String status;

    private EmployeeDto data;

    private String message;
}
