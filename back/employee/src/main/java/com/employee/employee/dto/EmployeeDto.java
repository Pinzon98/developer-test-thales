package com.employee.employee.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class EmployeeDto {

    private Long id;

    private String employee_name;

    private Double employee_salary;

    private Double employee_anual_salary;

    private Integer employee_age;

    private String profile_image;
}
