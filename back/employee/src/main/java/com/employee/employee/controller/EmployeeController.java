package com.employee.employee.controller;

import com.employee.employee.dto.EmployeeDto;
import com.employee.employee.dto.EmployeeResponseDto;
import com.employee.employee.dto.EmployeesResponseDto;
import com.employee.employee.model.Employee;
import com.employee.employee.service.IEmployeeService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(path="/employees", produces = {MediaType.APPLICATION_JSON_VALUE})
@AllArgsConstructor
@CrossOrigin(origins = "http://localhost:4200")
public class EmployeeController {

    private final IEmployeeService iEmployeeService;

    @GetMapping
    public ResponseEntity<EmployeesResponseDto> getAllEmployees() {
        return new ResponseEntity<>(iEmployeeService.getAllEmployees(), HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<EmployeeResponseDto> getEmployeeById(@PathVariable Long id) {
        EmployeeResponseDto employee = iEmployeeService.getEmployeeById(id);
        if (employee != null) {
            return new ResponseEntity<>(employee, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
}
