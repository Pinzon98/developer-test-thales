package com.employee.employee.service;

import com.employee.employee.dto.EmployeeDto;
import com.employee.employee.dto.EmployeeResponseDto;
import com.employee.employee.dto.EmployeesResponseDto;

import java.util.List;

public interface IEmployeeService {

    EmployeesResponseDto getAllEmployees();

    EmployeeResponseDto getEmployeeById(Long id);
}
