package com.employee.employee.mapper;

import com.employee.employee.dto.EmployeeDto;
import com.employee.employee.model.Employee;

public class EmployeeMapper {

    public static EmployeeDto mapToEmployeeDto(Employee employee, EmployeeDto employeeDto){
        return employeeDto;
    }

    public static Employee mapToEmployee(EmployeeDto employeeDto, Employee employee){
        return employee;
    }
}
