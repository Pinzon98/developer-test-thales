package com.employee.employee.dto;

import lombok.Data;

import java.util.List;

@Data
public class EmployeesResponseDto {

    private String status;

    private List<EmployeeDto> data;

    private String message;
}
