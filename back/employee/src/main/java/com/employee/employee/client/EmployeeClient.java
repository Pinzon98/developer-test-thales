package com.employee.employee.client;

import com.employee.employee.dto.EmployeeResponseDto;
import com.employee.employee.dto.EmployeeDto;
import com.employee.employee.dto.EmployeesResponseDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@Component
public class EmployeeClient {
    private static final String BASE_URL = "http://dummy.restapiexample.com/api/v1";

    @Autowired
    private RestTemplate restTemplate;

    public EmployeesResponseDto getAllEmployees() {
        EmployeesResponseDto employeesResponseDto = new EmployeesResponseDto();
        try {
            employeesResponseDto = restTemplate.getForObject(BASE_URL + "/employees", EmployeesResponseDto.class);
        }
        catch (Exception e){
            employeesResponseDto.setStatus("faild");
            employeesResponseDto.setMessage(e.getMessage());
        }
        return employeesResponseDto;
    }

    public EmployeeResponseDto getEmployeeById(Long id) {
        EmployeeResponseDto employeeResponseDto = new EmployeeResponseDto();
        try {
            employeeResponseDto = restTemplate.getForObject(BASE_URL + "/employee/" + id, EmployeeResponseDto.class);
        }
        catch (Exception e){
            employeeResponseDto.setStatus("faild");
            employeeResponseDto.setMessage(e.getMessage());
        }
        return employeeResponseDto;
    }
}
