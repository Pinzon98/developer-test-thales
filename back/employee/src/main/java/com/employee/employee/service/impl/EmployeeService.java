package com.employee.employee.service.impl;

import com.employee.employee.client.EmployeeClient;
import com.employee.employee.dto.EmployeeDto;
import com.employee.employee.dto.EmployeeResponseDto;
import com.employee.employee.dto.EmployeesResponseDto;
import com.employee.employee.model.Employee;
import com.employee.employee.service.IEmployeeService;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@AllArgsConstructor
public class EmployeeService implements IEmployeeService {

    private final EmployeeClient employeeClient;

    @Override
    public EmployeesResponseDto getAllEmployees() {
        EmployeesResponseDto employeeDtoList = employeeClient.getAllEmployees();
        if(employeeDtoList.getData() != null && !employeeDtoList.getData().isEmpty()){
            employeeDtoList.getData().forEach(employee -> employee.setEmployee_anual_salary(calculateAnualSalary(employee.getEmployee_salary())));
        }
        return employeeDtoList;
    }

    @Override
    public EmployeeResponseDto getEmployeeById(Long id) {
        EmployeeResponseDto employeeDto = employeeClient.getEmployeeById(id);
        if(employeeDto.getData() != null){
            employeeDto.getData().setEmployee_anual_salary(calculateAnualSalary(employeeDto.getData().getEmployee_salary()));
        }
        return employeeDto;
    }

    public Double calculateAnualSalary(Double salary){
        return salary * 12;
    }
}
